<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal("Shaun");

    echo "Nama Hewan : " . $sheep->name . "<br>";
    echo "Jumlah Kaki : " . $sheep->legs . "<br>";
    echo "Hewan Berdarah Dingin : " . $sheep->cold_blooded . "<br>"; 
    
    echo "------------------ <br>";

    $frog = new frog("Katak"); 
    echo "Nama Hewan : " . $frog->name . "<br>";
    echo "Jumlah Kaki : " . $frog->legs . "<br>";
    echo "Hewan Berdarah Dingin : " . $frog->cold_blooded . "<br>";
    echo "Katak Loncat : " . $frog->jump() . "<br>";

    echo "------------------ <br>";

    $ape = new ape("Kera Sakti"); 
    echo "Nama Hewan : " . $ape->name . "<br>";
    echo "Jumlah Kaki : " . $ape->legs . "<br>";
    echo "Hewan Berdarah Dingin : " . $ape->cold_blooded . "<br>";
    echo "Kera Loncat : " . $ape->yell() . "<br>";
