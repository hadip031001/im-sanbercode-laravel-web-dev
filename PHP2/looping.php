<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1> Contoh Looping </h1>
    <?php
    echo "<h3>Contoh Soal 1</h3>";

    echo "<h4>Looping 1</h4>";
    $i = 2;

    do{
        echo "$i - I Love Php <br>";
        $i +=2;
    } while ($i <=20);

    echo "<h4>Looping 2 <h4>";
    for ($a = 20; $a >= 2; $a-=2) {
        echo "$a - I Love PHP <br>";
    }
    
    echo "<h3>Contoh Soal 2 </h3>";

    $number = [18, 45, 29, 61, 47, 34];

    echo "Array Number : ";
    print_r($number);
    echo"<br>";
    echo "Array Sisa Baginya Adalah : ";
    foreach($number as $value) {
        $rest[] = $value %=5;
    }

    print_r($rest);

    echo "<h3>Contoh Soal 3</h3>";

    $items = [
        ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
        ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"]
    ];

    foreach($items as $indexItems){
        $tampung = [
            "id" => $indexItems[0],
            "nama" => $indexItems[1],
            "price" => $indexItems[2],
            "description" => $indexItems[3],
            "source" => $indexItems[4],
        ];

        print_r($tampung);
        echo "<br>";
    }

    echo "<h3>Contoh Soal 4</h3>";

    for ($j = 1; $j <= 5; $j++) {
        for($k = 1; $k <= $j; $k++) {
            echo "*";
        }
        echo "<br>";
    }
    ?>
    
</body>
</html>