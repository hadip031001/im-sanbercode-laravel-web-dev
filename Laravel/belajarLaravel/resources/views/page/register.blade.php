<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/home"method="POST">
        @csrf
        <label> Nama Depan :</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label> Nama Belakang :</label> <br>
        <input type="text" name="lname"> <br> <br>

        <label>Gender: </label> <br> <br>
        <input type="radio" value="1" name="Gender:"> Male <br>
        <input type="radio" value="2" name="Gender:"> Female <br>
        <input type="radio" value="3" name="Gender:"> Other <br> <br>

        <label>Nationality: </label> <br> <br>
        <select name ="Nationality">
            <option value="1">Indonesian</option>
            <option value="2"> Korean</option>
            <option value="3"> France</option>
        </select> <br> <br>

        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" value="1" name="Language Spoken:"> Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="Language Spoken:"> English <br>
        <input type="checkbox" value="3" name="Language Spoken:"> Other <br><br>

        <label>Bio:</label> <br> <br>
        <textarea name="Bio:" cols="30" rows="10"></textarea> <br> <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
